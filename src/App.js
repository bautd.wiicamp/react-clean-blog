/* eslint-disable react/jsx-props-no-spreading */
import { lazy, Suspense } from "react";
import { Switch } from "react-router-dom";
import PublicRoute from "route/PublicRoute";
import PrivateRoute from "route/PrivateRoute";

import loading from "helpers/loading";
// @TODO: Remember to change CSS here!
import "./App.css";

import NavBar from "views/layouts/nav-bar";
import Footer from "views/layouts/footer";

const UserHomepage = lazy(() => import("views/pages/user-homepage"));
const LoginPage = lazy(() => import("views/pages/login"));
const WelcomePage = lazy(() => import("views/pages/welcome-page"));

function App() {
  return (
    <div className="App">
      <NavBar />
      <main>
        <Suspense fallback={loading()}>
          <Switch>
            <PrivateRoute exact path="/homepage" component={UserHomepage} />
            <PublicRoute exact path="/login" component={LoginPage} />
            <PublicRoute exact path="/" component={WelcomePage} />
          </Switch>
        </Suspense>
      </main>
      <Footer />
    </div>
  );
}

export default App;
