/**
 * get an object of URL Queries
 *
 * @params: the query string
 *
 * @returns {object}
 */
export default function getObjectOfURLQueries(queryString) {
  return Object.fromEntries(new URLSearchParams(queryString));
}
